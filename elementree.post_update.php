<?php

/**
 * @file
 * Contains update functions for Elementree.
 */

/**
 * Implements hook_post_update_NAME() for setting default value for property type field.
 */
function elementree_post_update_set_default_property(&$sandbox) {
  // Get all available languages on the site.
  $languages = \Drupal::languageManager()
    ->getLanguages();

  try {
    // Need to load all existing elementree paragraphs on the site.
    $entityTypeManager = \Drupal::entityTypeManager()
      ->getStorage('paragraph');

    /** @var \Drupal\paragraphs\ParagraphInterface[] $paragraphs */
    $paragraphs = $entityTypeManager
      ->loadByProperties([
        'type' => 'elementree_label',
      ]);

    foreach ($paragraphs as $paragraph_id => $paragraph) {
      // Need to be sure if property type field already exists.
      if ($paragraph->hasField('field_property_type')) {
        // Elementree Label paragraph can have translation for each language.
        foreach ($languages as $language) {
          if ($paragraph->hasTranslation($language->getId())) {
            $paragraph = $paragraph->getTranslation($language->getId());

            // If translation exist, and property type field is empty.
            // Need to provide default value.
            if ($paragraph->get('field_property_type')->isEmpty()) {
              $paragraph
                ->set('field_property_type', 'plain')
                ->save();
            }
          }
        }
      }
    }
  }
  catch (Exception $exception) {
    \Drupal::logger('Elementree')
      ->error("Post update exception: {$exception}");
  }
}
