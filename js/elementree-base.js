/**
 * @file
 * CRRender implementation.
 */

/**
 * CRRender function.
 *
 * @param element
 *   Element in DOM.
 * @param component_name
 *   Component name.
 * @param settings
 *   Configuration and props.
 * @constructor
 */
function CRRender(element, component_name, settings) {
  // Use try/catch for any error.
  try {
    if (CRWidgets[component_name]) {
      // Render component.
      CRWidgets[component_name](element, settings);
    }
    else {
      // Show the problem.
      element.innerHTML = 'Block ' + "<strong>" + component_name + "</strong>" + ' is empty client component';
    }
  }
  catch (exception) {
    // Show the error.
    element.innerHTML = 'Client component is broken.' + "<br/>" + exception;
  }
}
