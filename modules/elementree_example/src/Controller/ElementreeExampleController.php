<?php

namespace Drupal\elementree_example\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Example module.
 */
class ElementreeExampleController extends ControllerBase {

  /**
   * Returns an example of component created by Elementree module.
   *
   * @return array
   *   Render array of example component.
   */
  public function buildExampleComponent() {
    $config = [];
    $item = elementree_item('MyPageComponentName', $config);
    $item['#attached']['library'][] = 'elementree_example/elementree_example.components';
    return $item;
  }
}