<?php

namespace Drupal\elementree_example\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'ElementreeExampleBlock' block.
 *
 * @Block(
 *   id = "elementree_example_block",
 *   admin_label = @Translation("Elementree Example block"),
 *   category = @Translation("Elementree")
 * )
 */
class ElementreeExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['count'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Count'),
      '#default_value' => $config['count'] ?? '',
    ];

    $form['section_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Section title'),
      '#default_value' => $config['section_title'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('count', $form_state->getValue('count'));
    $this->setConfigurationValue('section_title', $form_state->getValue('section_title'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $item = elementree_item('MyComponentName', $config);
    $item['#attached']['library'][] = 'elementree_example/elementree_example.components';
    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
