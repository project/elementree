<?php

namespace Drupal\elementree;

use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Elementree paragraph fields.
 */
const ELEMENTREE_BLOCK_FIELD_LABELS = 'field_elementree_block_labels';
const ELEMENTREE_BLOCK_FIELD_COMPONENT_NAME = 'field_elementree_block_component';
const ELEMENTREE_BLOCK_FIELD_REFERENCE_BLOCK = 'field_ref_elementree_block';
const ELEMENTREE_BLOCK_PARAGRAPH_FIELD_KEY = 'field_key';


/**
 * ElementreeBaseProvider service.
 *
 * @package Drupal\elementree
 */
class ElementreeBaseProvider implements ElementreeBaseProviderInterface {

  /**
   * The current language.
   *
   * @var string
   */
  public string $currentLanguage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  public EntityRepository $entityRepository;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  public FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepository $entityRepository,
    FileUrlGeneratorInterface $file_url_generator,
  ) {
    $this->currentLanguage = $language_manager->getCurrentLanguage()->getId();
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entityRepository;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockParams($block_content) {
    $data = [
      'component_name' => '',
      'labels' => [],
    ];

    if (!empty($block_content) && $block_content instanceof BlockContentInterface) {
      if (!$block_content->get(ELEMENTREE_BLOCK_FIELD_COMPONENT_NAME)->isEmpty()) {
        $data['component_name'] = $block_content
          ->get(ELEMENTREE_BLOCK_FIELD_COMPONENT_NAME)
          ->getString();
      }

      if (!$block_content->get(ELEMENTREE_BLOCK_FIELD_LABELS)->isEmpty()) {
        /** @var \Drupal\paragraphs\Entity\Paragraph[] $labels_paragraphs */
        $labels_paragraphs = $block_content
          ->get(ELEMENTREE_BLOCK_FIELD_LABELS)
          ->referencedEntities();

        foreach ($labels_paragraphs as $labels_paragraph) {
          if ($labels_paragraph->hasTranslation($this->currentLanguage)) {
            $labels_paragraph = $labels_paragraph
              ->getTranslation($this->currentLanguage);
          }

          if (!empty($labels_paragraph)) {
            if ($labels_paragraph->hasField('field_property_type')
              && $field_type = $labels_paragraph->get('field_property_type')
                ->getString()) {

              $labels_paragraph_values = $this
                ->getLabelValue($labels_paragraph, $field_type);

              if (!empty($labels_paragraph_values)) {
                $label_key = $labels_paragraph
                  ->get(ELEMENTREE_BLOCK_PARAGRAPH_FIELD_KEY)
                  ->getString();
                $data['labels'][$label_key] = $labels_paragraph_values;
              }
            }
          }
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockDependenciesParams($block_content) {
    $data = [];

    if (!empty($block_content) && $block_content instanceof BlockContentInterface) {
      if (!$block_content->get(ELEMENTREE_BLOCK_FIELD_REFERENCE_BLOCK)->isEmpty()) {
        $dependent_blocks = $block_content
          ->get(ELEMENTREE_BLOCK_FIELD_REFERENCE_BLOCK)
          ->referencedEntities();

        /** @var \Drupal\block_content\Entity\BlockContent[] $dependent_blocks */
        foreach ($dependent_blocks as $dependent_block) {
          $dependent_block_component_name = $dependent_block
            ->get(ELEMENTREE_BLOCK_FIELD_COMPONENT_NAME)
            ->getString();

          $data[$dependent_block_component_name] = $this
            ->getBlockParams($dependent_block);
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabelValue(ParagraphInterface $labels_paragraph, string $field_type) {
    $label_values = [];

    $field_machine_name = $field_type == 'long' || $field_type == 'image' ?
      'field_' . $field_type . '_value' : 'field_value';

    if ($labels_paragraph->hasField($field_machine_name) &&
      !$labels_paragraph->get($field_machine_name)->isEmpty()) {

      $label_values = $labels_paragraph
        ->get($field_machine_name)
        ->getValue();

      if ($field_machine_name === 'field_image_value') {
        foreach ($label_values as $label_key => $label_value) {
          if (!empty($label_value['target_id'])) {
            /** @var \Drupal\media\MediaInterface $media */
            $media = $this->entityTypeManager
              ->getStorage('media')
              ->load($label_value['target_id']);

            if ($file_id = $media->get('field_media_image')[0]->getValue()['target_id']) {
              /** @var \Drupal\file\FileInterface $file */
              $file = $this->entityTypeManager
                ->getStorage('file')
                ->load($file_id);

              if (!empty($file)) {
                $absolute_path = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
                $label_values[$label_key] = $this->fileUrlGenerator->transformRelative($absolute_path);
              }
              else {
                unset($label_key[$label_key]);
              }
            }
          }
        }
      }

      // Remove 'value' key, if there single value.
      if (count($label_values) === 1) {
        $label_values = $field_machine_name === 'field_image_value' ?
          $label_values[0] : $label_values[0]['value'];
      }
    }

    return $label_values;
  }

  /**
   * {@inheritdoc}
   */
  public function getBidByComponent(string $component) {
    /** @var \Drupal\block_content\Entity\BlockContent[] $block */
    $block = $this->entityTypeManager
      ->getStorage('block_content')
      ->loadByProperties([
        ELEMENTREE_BLOCK_FIELD_COMPONENT_NAME => $component,
      ]);

    $block = reset($block) instanceof BlockContentInterface ?
      reset($block) : NULL;

    if (!empty($block) && $block->hasTranslation($this->currentLanguage)) {
      $block = $block->getTranslation($this->currentLanguage);
    }

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function getTermsByVid(array $vocabularies) {
    $term_data = [];

    $termEntityStorage = $this->entityTypeManager
      ->getStorage('taxonomy_term');

    foreach ($vocabularies as $key => $value) {
      /** @var object[]|\Drupal\taxonomy\TermInterface[] $terms */
      $terms = $termEntityStorage
        ->loadTree($value);

      foreach ($terms as $term) {
        /** @var \Drupal\taxonomy\TermInterface $taxonomy_term */
        $taxonomy_term = $termEntityStorage->load($term->tid);
        /** @var \Drupal\taxonomy\TermInterface $taxonomy_term_trans */
        $taxonomy_term_trans = $this->entityRepository
          ->getTranslationFromContext($taxonomy_term, $this->currentLanguage);

        if ($taxonomy_term_trans) {
          $term_data[$value][] = [
            'tid' => $term->tid,
            'name' => $taxonomy_term_trans->getName(),
          ];
        }
      }
    }

    return $term_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodesByType(array $types) {
    $node_data = [];

    foreach ($types as $type) {
      $nodes = $this->entityTypeManager
        ->getStorage('node')
        ->loadByProperties([
          'type' => $type,
        ]);

      foreach ($nodes as $node) {
        $node_trans = $this->entityRepository
          ->getTranslationFromContext($node, $this->currentLanguage);

        if ($node_trans) {
          $node_data[$type][] = [
            'nid' => $node->id(),
            'name' => $node_trans->label(),
          ];
        }
      }
    }

    return $node_data;
  }

}
