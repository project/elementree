<?php

namespace Drupal\elementree;

use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Interface ElementreeBaseProviderInterface.
 *
 * @package Drupal\elementree
 */
interface ElementreeBaseProviderInterface {

  /**
   * Constructs a ElementreeBaseProviderInterface object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityRepository $entityRepository
   *   Entity repository.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepository $entityRepository,
    FileUrlGeneratorInterface $file_url_generator);

  /**
   * Gets params for React Components from Elementree block.
   *
   * @param \Drupal\block_content\Entity\BlockContent|null $block_content
   *   Elementree block content.
   *
   * @return array
   *   List of Elementree block params:
   *   - 'component_name': Name of the Elementree component item.
   *   - 'labels': List of element keys with labels.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBlockParams($block_content);

  /**
   * Gets params for React Components from Elementree block dependencies.
   *
   * @param \Drupal\block_content\Entity\BlockContent|null $block_content
   *   Elementree block content.
   *
   * @return array
   *   List of depend blocks params:
   *   - 'component_name': Name of the Elementree component item.
   *   - 'labels': List of element keys with labels.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBlockDependenciesParams($block_content);

  /**
   * Helper function for returning values from Elementree Labels.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $labels_paragraph
   *   The instance of Elementree Label paragraph.
   * @param string $field_type
   *   Type of field, which should be returned.
   *
   * @return array
   *   Array of values or empty array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLabelValue(ParagraphInterface $labels_paragraph, string $field_type);

  /**
   * Return block by component name.
   *
   * @param string $component
   *   Component name.
   *
   * @return \Drupal\block_content\Entity\BlockContent|null
   *   Return block or null if block doesn't exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getBidByComponent(string $component);

  /**
   * Create array with terms by term vocabulary.
   *
   * @param array $vocabularies
   *   Type of vocabulary.
   *
   * @return array
   *   Array of term identifier and term title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTermsByVid(array $vocabularies);

  /**
   * Create array with nodes by node type.
   *
   * @param array $types
   *   Type of node.
   *
   * @return array
   *   Array of node identifier and node title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNodesByType(array $types);

}
