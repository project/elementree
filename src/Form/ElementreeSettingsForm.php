<?php

namespace Drupal\elementree\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides ElementreeSettingsForm.
 */
class ElementreeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'elementree_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#tree'] = TRUE;
    $config = $this->config('elementree.settings')
      ->get('config');

    $form['settings']['components_js_path'] = [
      '#type' => 'textarea',
      '#title' => t('Components js path'),
      '#default_value' => $config['components_js_path'],
    ];

    $form['settings']['components_css_path'] = [
      '#type' => 'textarea',
      '#title' => t('Components css path'),
      '#default_value' => $config['components_css_path'],
    ];

    $form['settings']['reset_ssr'] = [
      '#type' => 'checkbox',
      '#title' => t('Delete server side rendering in browser'),
      '#default_value' => $config['reset_ssr'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_value = $form_state->getValue('settings');
    $this->configFactory()
      ->getEditable('elementree.settings')
      ->set('config', $form_value)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['elementree.settings'];
  }

}
